const express=require('express')
const router=express.Router()
const associateContoller=require('../controllers/associate.controller')
router.post('/',associateContoller.creatAssociate)
router.get('/',associateContoller.getAssociateWithSpecialization),
router.delete('/:id',associateContoller.deleteAssociate)
router.patch("/:id",associateContoller.associateUpdate)
module.exports=router