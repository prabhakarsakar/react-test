const express=require('express')
const router=express.Router()
const specializationController=require("../controllers/Spcialization.controller")

router.post('/',specializationController.createSpecialization)
router.patch("/:id",specializationController.updateSpecialization)
router.get("/",specializationController.getAllSpecialization)

module.exports=router