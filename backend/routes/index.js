const express=require('express')
const router=express.Router()
const associate=require("./associate.router")
const specialization=require("./specialization.controller")
router.use("/associate",associate)
router.use("/specialization",specialization)
module.exports=router