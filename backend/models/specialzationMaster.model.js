const db = require("../connections/db")
const Sequelize = require('sequelize')
const specializationMaster = db.define("specialization_master", {
    specializationId: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        allowNull: false,
        primaryKey: true
    },
    specializationName:{type:Sequelize.STRING}
});
module.exports = specializationMaster





