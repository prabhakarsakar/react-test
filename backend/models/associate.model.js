const db = require("../connections/db")
const Sequelize = require('sequelize')

const asspciate = db.define("associate", {
    associateId: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        allowNull: false,
        primaryKey: true
    },
    associateName: { type: Sequelize.STRING },
    phone: { type: Sequelize.STRING },
    address:{type: Sequelize.STRING},
    image:{type: Sequelize.STRING},

});

module.exports = asspciate





