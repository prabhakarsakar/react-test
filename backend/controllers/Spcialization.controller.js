const Joi = require('joi')
const Sequelize = require('sequelize')
const Op = Sequelize.Op
const Specialization = require('../models/specialzationMaster.model')
const createSpecialization = async (req, res) => {
    const Schema = Joi.object({
        specializationName: Joi.string().required(),
    });
    let validateSchema = Schema.validate(req.body);
    let userPayload;
    if (validateSchema.error) {
        return res.status(400).json({
            message: validateSchema.error.message || "Bad Request",
            status: 400
        })
    } else {
        userPayload = validateSchema.value;
    }
    try {
        const exists = await Specialization.findOne({where: {specializationName: userPayload.specializationName}})
        console.log(exists);
        if (exists) {
            return res.status(409).send({
                message: "specialization Name alreay ",
                status: 409,
            })
        } else {
            const result1 = await Specialization.create(userPayload)
            return res.status(200).send({
                message: "specialization added successfully!",
                status: 200,
            })
        }
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            message: 'Internal Server Error',
            status: 500
        })
    }
}
const updateSpecialization = async (req, res) => {
    const id=req.params.id
    const Schema = Joi.object({
        specializationName: Joi.string().required(),
    });
    let validateSchema = Schema.validate(req.body);
    let userPayload;
    if (validateSchema.error) {
        return res.status(400).json({
            message: validateSchema.error.message || "Bad Request",
            status: 400
        })
    } else {
        userPayload = validateSchema.value;
    }
    // console.log(userPayload, "nnnnnnnnnnnnnnnnnnnnnnnnnnn");
    try {
        const exists = await Specialization.update(payload,{ where: { specializationId: req.params.id } })
        console.log(exists);
        if (exists == 0) {
            return res.status(404).send({
                message: "This specialization doesn't exist with this id ",
                status: 404,
            })
        } else {
            return res.status(200).send({
                message: "update successfully!",
                status: 200,
            })
        }
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            message: 'Internal Server Error',
            status: 500
        })
    }
}

const getAllSpecialization=async(req,res)=>{
    try{
        const result =await Specialization.findAll({})
        if (!result || result.length==0) {
            return res.status(404).send({
                message: "data not found ",
                status: 404,
            })
        } else {
            return res.status(200).send({
                message: "access successfully!",
                data:result,
                status: 200,
            })
        }
        
    }catch(error){
        return res.status(500).json({
            message: 'Internal Server Error',
            status: 500
        })
    }
}

module.exports={createSpecialization,updateSpecialization,getAllSpecialization}