const Joi = require('joi')
const Sequelize = require('sequelize')
const Op = Sequelize.Op
const associatetable = require('../models/associate.model')
const Specialization = require('../models/specialzationMaster.model')
const specializationtable = require('../models/Spcialization.model')
const creatAssociate = async (req, res) => {
  const Schema = Joi.object({
    image:Joi.string().optional().allow(null,''),
    associateName: Joi.string()
      .required()
      .messages({ 'string.pattern.base': `Invalid Associate Name .` })
      .min(3)
      .max(30),
    phone: Joi.string()
      .regex(/^[0-9]{10}$/)
      .messages({ 'string.pattern.base': `Invalid phone Number.` })
      .required(),
    address: Joi.string().required(),
    specializationId: Joi.array()
      .items()
      .optional()
  })
  let validSchema = Schema.validate(req.body)
  if (validSchema.error) {
    return res.status(400).json({
      message: validSchema.error.message || 'Bad Request',
      status: 400
    })
  } else {
    validSchema = validSchema.value
  }
  try {
    let specializationpayload = validSchema.specializationId
    delete validSchema.specializationId
    const exist = await associatetable.findOne({
      where: {
        [Op.or]: [
          { associateName: validSchema.associateName },
          { phone: validSchema.phone }
        ]
      }
    })
    if (exist) {
      return res.status(409).send({
        message: 'associate already exist',
        status: 409
      })
    }
    const result = await associatetable.create(validSchema)
    if (specializationpayload) {
      for (i of specializationpayload) {
        const specializePayload = {
          specializationId: i,
          associateId: result.dataValues.associateId
        }
        const Spcialization = await specializationtable.create(
          specializePayload
        )
      }
    }
    return res.status(201).send({
      message: 'associate master added successfully !',
      status: 201
    })
  } catch (err) {
    console.log(err)
    return res.status(500).json({
      message: 'Internal Server Error',
      status: 500
    })
  }
}
const getAssociateWithSpecialization = async (req, res) => {
  const Query = Joi.object({
    associateName: Joi.string()
      .optional()
      .allow(null, '')
  })
  let validSchema = Query.validate(req.query)
  if (validSchema.error) {
    return res.status(400).json({
      message: validSchema.error.message || 'Bad Request',
      status: 400
    })
  } else {
    validSchema = validSchema.value
  }
  let filter = {}
  if (validSchema.associateName) {
    filter = {
      ...filter,
      [Op.or]: [
        { associateName: { [Op.like]: `%${req.query.associateName}%` } },

      ]
    };
  }
  try {
    associatetable.hasMany(specializationtable, { foreignKey: 'associateId' })
    specializationtable.hasOne(Specialization, {
      foreignKey: 'specializationId'
    })
    associatetable.belongsTo(specializationtable, { foreignKey: 'associateId' })
    specializationtable.belongsTo(Specialization, {
      foreignKey: "specializationId"
    })
    const result = await associatetable.findAll({
      where: filter,
      attributes:['associateName','associateId','phone','address','image'],
      include: [
        {
          model: specializationtable,
          attributes: ['specializationId'],
          include: [
            {
              model: Specialization,
              attributes: ['specializationName']
            }
          ]
        }
      ]
    })
    if (!result || result.length == 0) {
      return res.status(404).json({
        message: 'Data Not Found',
        status: 404
      })
    } else {
      return res.status(200).json({
        message: 'Access Successfully',
        data: result,
        status: 202
      })
    }
  } catch (err) {
    console.log(err,';;;;;;;;;;;;;');
    return res.status(500).json({
      message: 'Internal Server Error',
      status: 500
    })
  }
}

const deleteAssociate = async (req, res) => {
  try {
    const id = req.params.id
    const result = await associatetable.destroy({ where: { associateId: id } })
    if (result == 0) {
      return res.status(404).json({
        message: "associate doesn't exist with this Id",
        status:404
      })
    } else {
      const specialization = await specializationtable.destroy({
        where: { associateId: id }
      })
      return res.status(202).json({
        message: 'deleted successfully',
        status: 200
      })
    }
  } catch (err) {
    console.log(err);
    return res.status(500).json({
      message: 'Internal Server Error',
      status: 500
    })
  }
}

const associateUpdate = async (req, res) => {
  const id = req.params.id
  const Schema = Joi.object({
    image:Joi.string().optional().allow(null,""),
    associateId:Joi.string().optional().allow(null,""),
    specializations:Joi.array().optional().allow(null,""),


    associateName: Joi.string()
      .messages({ 'string.pattern.base': `Invalid Associate Name.` })
      .min(3)
      .max(30),
    phone: Joi.string()
      .regex(/^[0-9]{10}$/)
      .messages({ 'string.pattern.base': `Invalid phone Number.` }),
    address: Joi.string(),
    specializationId: Joi.array()
      .items()
      .optional()
  })
  let validSchema = Schema.validate(req.body)
  if (validSchema.error) {
    return res.status(400).json({
      message: validSchema.error.message || 'Bad Request',
      status: 400
    })
  } else {
    validSchema = validSchema.value
  }
  try {
    let specializationPayload = validSchema.specializationId
    delete validSchema.specializationId

    const result = await associatetable.update(validSchema,{ where: { associateId: id } })
    if (!result) {
      return res.status(404).json({
        message: "associate doesn't exist with this Id"
      })
    } else {
      console.log(result);
      const deleteSpec = await specializationtable.destroy({where: { associateId:id }})
      if (specializationPayload) {
        for (i of specializationPayload) {
          const specializePayload = {
            specializationId: i,
            associateId: id
          }
          const Spcialization = await specializationtable.create(
            specializePayload
          )
        }
      }
      return res.status(200).json({
        message: "associate update successfully",
        status:200
      })
    }
  } catch (err) {
    console.log(err);
    return res.status(500).json({
      message: 'Internal Server Error',
      status: 500
    })
  }
}

module.exports = {
  creatAssociate,
  deleteAssociate,
  getAssociateWithSpecialization,
  associateUpdate
}
