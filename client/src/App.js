import { React } from 'react'
import './App.css';
import {
  BrowserRouter as Router,
  Routes,
  Route,
} from "react-router-dom"
import UsersPage from './pages/users';
function App() {
  return (
    <div className="App">
      <Router>
        <Routes>
          <Route exect path="/" element={<UsersPage />} />
        </Routes>
      </Router>
    </div>
  );
}

export default App;