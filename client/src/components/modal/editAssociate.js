import React, { useEffect, useState } from 'react'
import { useTheme } from '@mui/material/styles';
import OutlinedInput from '@mui/material/OutlinedInput';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import { getSpecialization, addUser, getUsers, updateUser } from '../../services/userService';







export default function EditAssociate(props) {
    console.log(props)
        const [userData, setUserdata] = useState(props.associateData)
    const [specializationNames, setSpecializationNames] = useState()
    const [specialization, setSpecialization] = useState(props.associateData.specializations.map((itemss) => itemss.specializationId));

    const ITEM_HEIGHT = 48;
    const ITEM_PADDING_TOP = 8;
    const theme = useTheme();

    const MenuProps = {
        PaperProps: {
            style: {
                maxHeight: ITEM_HEIGHT * 4 + ITEM_PADDING_TOP,
                width: 250,
            },
        },
    };


    function getStyles(name, personName, theme) {
        return {
            fontWeight:
                personName.indexOf(name) === -1
                    ? theme.typography.fontWeightRegular
                    : theme.typography.fontWeightMedium,
        };
    }

    const handleChange = (event) => {
        const { target: { value }, } = event;
        setSpecialization(
            typeof value === 'string' ? value.split(',') : value,
        );
    };
    useEffect(() => {
        const response = getSpecialization()

        response
            .then((data) => {
                setSpecializationNames(data?.data?.data)
                console.log(data?.data?.data, "jjjjjjjjjjjjj");
            }).catch(error => {
                console.log({ error });
            })
    }, [])
    console.log(specializationNames, 'gggggggggggg');
    const onChangeUserData = (e) => {
        const key = e.target.id
        const value = e.target.value
        setUserdata({ ...userData, [key]: value })
    }

    const submitFun = async () => {
        const payload = {
            ...userData,
            specializationId: specialization
        }
        console.log(payload, "payload");
        const add = await updateUser(props.associateId,payload)
        console.log(add, 'add');
        const getuser = await getUsers()
        console.log(getuser, 'getuser');
        props.afterAdd(getuser?.data?.data)
    }

    return (
        <div>
            <button class="btn-sm border btn-dark  px-4  " data-toggle="modal" data-target={`#editModal${props.associateId}`}>edit </button>


            <div class="modal fade" id={`editModal${props.associateId}`} tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content" >
                        <div class="modal-header">
                            <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Edit Associate</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">

                            <div className='col-12 d-flex flex-column  mb-3'>
                                <label htmlFor="name" className="mb-0 imp">Name</label>
                                <input
                                    id='associateName'
                                    value={userData?.associateName}
                                    placeholder="N/A"
                                    type="text"
                                    onChange={(e) => onChangeUserData(e)}
                                />
                            </div>

                            <div className='col-12 d-flex flex-column  mb-3'>
                                <label htmlFor="phone" className="mb-0">Phone Number</label>
                                <input
                                    id='phone'
                                    value={userData?.phone}
                                    type="text"
                                    placeholder="N/A"
                                    onChange={(e) => onChangeUserData(e)}
                                />
                            </div>

                            <div className='col-12 d-flex flex-column  mb-3'>
                                <label htmlFor="address" className="mb-0">Address</label>
                                <input
                                    id='address'
                                    type="text"
                                    value={userData?.address}
                                    placeholder="N/A"
                                    onChange={(e) => onChangeUserData(e)}
                                />
                            </div>
                            <div>
                                <FormControl sx={{ m: 1, width: 300 }} disableEnforceFocus>
                                    <InputLabel id="demo-multiple-name-label">Specializations</InputLabel>
                                    <Select
                                        labelId="demo-multiple-name-label"
                                        id="demo-multiple-name"
                                        multiple
                                        value={specialization}
                                        onChange={(e) => handleChange(e)}
                                        input={<OutlinedInput label="Specializations" />}
                                        MenuProps={MenuProps}
                                    >
                                        {specializationNames?.map((item, index) => (
                                            <MenuItem
                                                key={index}
                                                value={item.specializationName}
                                                style={getStyles(item.specializationName, specialization, theme)}
                                            >
                                                {item.specializationName}
                                            </MenuItem>
                                        ))}
                                    </Select>
                                </FormControl>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary" onClick={() => submitFun()} data-dismiss="modal">Update</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )

}
