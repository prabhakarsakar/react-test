import {  Delete,Get,  Patch,  Post } from "../lib/request"

export const getUsers = (searchData="") => {
    return Get(`/associate?associateName=${searchData}`)
}

export const addUser = (data) => {
    return Post(`/associate`, data)
}

export const deleteUser = (id) => {
    return Delete(`/associate/${id}`)
}

export const updateUser = (id,data) => {
    return Patch(`/associate/${id}`, data)
}

// export const changeUserStatus = (data) => {
//     return DeleteWithToken(`/users`, data)
// }

export const getSpecialization = () => {
    return Get(`/specialization`)
}
