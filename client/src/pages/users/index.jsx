import { Edit } from "@mui/icons-material";
import React, { useEffect, useState } from "react";
import AddAssociate from "../../components/modal/addAssociate";
import EditAssociate from "../../components/modal/editAssociate";
import Button from '@mui/material/Button';
import SearchIcon from '@mui/icons-material/Search';

import { deleteUser, getUsers } from "../../services/userService";



const UsersPage = () => {
  const [data, setData] = useState([]);
  const [searchData, setSearchData] = useState('')

  useEffect(() => {
    const response = getUsers(searchData)
    response.then(data => {
      setData(data?.data?.data)
      console.log(data, 'data data data ');
    }).catch(err => {
      console.log({ err });
    })
  }, []);


  const afterAdd = (updatedData) => {
    console.log(updatedData, 'gsdfd');
    setData(updatedData)
  }

  const deletefun = async (id) => {
    console.log(id);
    const deleteData = await deleteUser(id)
    console.log(deleteData, 'data deleted successfully');
    const getuser = await getUsers()
    setData(getuser?.data?.data)

  }

  const onClickSearch = () => {
    const response = getUsers(searchData)
    response.then(data => {
      setData(data?.data?.data || [])
      console.log(data, 'data data data ');
    }).catch(err => {
      console.log({ err });
      setData( [])
    })
  }

console.log(data);
  return (
    <>



      <div className="col-12  d-flex justify-content-end">
        <div className=" d-flex mr-3 mt-3">
          <input
            class="form-control form-control-sm ml-3 mr-1 w-50 mt-1"
            type="text"
            placeholder="Search"
            aria-label="Search"
            onChange={(e) => setSearchData(e.target.value)}
          />
          <Button variant="contained" size="small" onClick={() => onClickSearch()}>
            <SearchIcon/>
          </Button>
        </div>


        <span className=" mr-3 mt-3" >
          <AddAssociate afterAdd={afterAdd} />
        </span>

      </div>
      <div >
        <div className="col-12 row py-3 ">
          {data && data.length==0 && <h1>No Data Found..!</h1>}
          {data.map((item, index) => (
            <div key={index} className="col-md-4 pb-3">
              <div class="card " style={{ width: "22rem" }}>
                <img
                  class="card-img-top"
                  src={item?.image || "ProImage.jpeg"}
                  alt="Card image cap"

                />
                <div class="card-body mr-1">
                  <div className="d-flex ">
                    <label
                      className="text-left  font-weight-bolder"
                      style={{ minWidth: "4rem" }}
                    >
                      Name :
                    </label>
                    <div className="text-wrap" style={{ width: " 12rem" }}>
                      {item?.associateName || "N/A"}
                    </div>
                  </div>
                  <div className="d-flex ">
                    <label
                      className="text-left  font-weight-bolder"
                      style={{ minWidth: "4rem" }}
                    >
                      Phone :
                    </label>
                    <div className="text-wrap" style={{ width: " 12rem" }}>
                      {item?.phone || "N/A"}

                    </div>
                  </div>
                  <div className="d-flex ">
                    <label
                      className="text-left  font-weight-bolder"
                      style={{ minWidth: "5rem" }}
                    >
                      Address :
                    </label>
                    <div className="text-wrap" style={{ width: " 12rem" }}>
                      {item?.address}
                    </div>
                  </div>
                  <div className="d-flex mr-1">
                    <label
                      className="text-left  font-weight-bolder"
                      style={{ minWidth: "8rem" }}
                    >
                      Spacialization :
                    </label>
                    <div className="text-wrap" style={{ width: " 11rem" }}>
                      {item?.specializations.map((itemss) => itemss.specializationId).toString() || "N/A"}
                    </div>
                  </div>
                  <br />

                  <div className="d-flex justify-content-between">
                    <button
                      class="btn-sm border  btn-danger px-4 "
                      // assoId={"mm"}
                      onClick={() => deletefun(item.associateId)}
                    >
                      Delete
                    </button>

                    <EditAssociate associateId={item.associateId} associateData={item} afterAdd={afterAdd} />
                    {/* <button class="btn-sm bord er btn-dark  px-4  " >edit </button> */}
                  </div>

                </div>
              </div>
            </div>
          ))}

        </div>
      </div>
    </>
  )

};

export default UsersPage;
