import Axios from 'axios';
import * as config from './config';


const getUrl = (endpoint) => {
    return config.API_HOST + endpoint;
}

export const Post = async (endpoint, data) => {
    return Axios.post(getUrl(endpoint), data, {
        headers: {
            'Content-Type': 'application/json',
            // 'language': LANG || 'en'
        }
    })
}

export const Get = async (endpoint, extraHeaders = {}) => {
    return Axios.get(getUrl(endpoint), {
        headers: {
            'Content-Type': 'application/json',
            // 'language': LANG || 'en',
            ...extraHeaders
        }
    })
}

export const Patch = async (endpoint, data) => {
    return Axios.patch(getUrl(endpoint),data, {
        headers: {
            'Content-Type': 'application/json',
            // 'language': LANG || 'en',
           
        }
    })
}
export const Put = async (endpoint, data) => {
    return Axios.put(getUrl(endpoint),data, {
        headers: {
            'Content-Type': 'application/json',
            // 'language': LANG || 'en',
           
        }
    })
}
export const Update = async (endpoint, data) => {
    return Axios.update(getUrl(endpoint),data, {
        headers: {
            'Content-Type': 'application/json',
            // 'language': LANG || 'en',
           
        }
    })
}

export const Delete = async (endpoint) => {
    return Axios.delete(getUrl(endpoint), {
        headers: {
            'Content-Type': 'application/json',
            // 'language': LANG || 'en',
           
        }
    })
}












